<?php
/**
 * Plugin Name:       WPFOSSBILLING
 * Plugin URI:        https://cyberwp.cloud
 * Description:       Manage multiple CyberPanel installations via WordPress.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Usman Nasir
 * Author URI:        https://cyberwp.cloud
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

require_once ('vendor/autoload.php');
require_once ('FOSSBillingAPI.php');

function crb_load()
{
    \Carbon_Fields\Carbon_Fields::boot();
    $config = array(
        'api_url' => 'https://demo.fossbilling.org/api',
        'api_token' => 'wNrioFGLIIo2LxbH0BAj7REcSwUMTBgN'
    );
    global $api;

    $api = new FOSSBillingAPI($config);
    require_once ('inc/settings/pluginsettings.php');
    add_action('carbon_fields_register_fields', 'crb_attach_theme_options');

    require_once ('inc/settings/subscriptionfields.php');
    add_action('woocommerce_product_data_tabs', 'simpleProductLicenseManagerTab');
    add_action('woocommerce_product_data_panels', 'simpleProductLicenseManagerPanel');
    add_action('woocommerce_admin_process_product_object', 'save_admin_product_custom_fields_values');
    add_action('fossbilling_product_data_fields', 'add_admin_product_custom_fields', 10, 3);

    require_once ('inc/settings/usermeta.php');
    add_action('carbon_fields_register_fields', 'WPFossBilling_UserMeta');

    require_once ('inc/woocommerce/payments.php');
    add_action('woocommerce_checkout_subscription_created', 'setupNewHostingAccount', 0, 3);
}

add_action('muplugins_loaded', 'crb_load');
