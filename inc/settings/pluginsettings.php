<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

function crb_attach_theme_options()
{
    Container::make('theme_options', __('FOSSBILLING'))
        ->add_fields(
            array(
                Field::make('text', 'api_url', 'API URL'),
                Field::make('text', 'api_token', 'API Token'),
            )
        );
}