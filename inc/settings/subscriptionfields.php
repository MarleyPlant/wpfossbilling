<?php

/**
 * Displays the new fields inside the new product data tab.
 */
function simpleProductLicenseManagerPanel()
{
    global $post;


    echo sprintf(
        '<div id="%s" class="panel woocommerce_options_panel"><div class="options_group">',
        'fossbilling_product_data'
    );

  

    do_action('fossbilling_product_data_fields', $post);

    echo '</div></div>';
}

/**
 * Adds a product data tab for simple WooCommerce products.
 *
 * @param array $tabs
 *
 * @return mixed
 */
function simpleProductLicenseManagerTab($tabs)
{
    $tabs['fossbilling_tab'] = array(
        'label' => __('FOSSBilling', 'fossbilling'),
        'target' => 'fossbilling_product_data',
        'class' => array('show_if_subscription'),
        'priority' => 21
    );

    return $tabs;
}

function get_hosting_plans()
{
    global $api;
    $plans = $api->products->getProducts();
    $options = array();
    foreach ($plans as $plan) {
        $plan_name = $plan->title;
        $plan_id = $plan->id;
        $options[$plan_id] = $plan_name;
    }

    return $options;
}

// Showing custom fields on admin product settings "general" tab
function add_admin_product_custom_fields()
{
    global $post;

    $product = wc_get_product($post->ID);

    echo '<div class="product_custom_field show_if_subscription">';

    woocommerce_wp_checkbox(
        array(
            'id' => 'is_hosting_plan',
            'name' => 'is_hosting_plan',
            'label' => __('Connect To FossBilling', 'woocommerce'),
            'value' => $product->get_meta('is_hosting_plan') ? $product->get_meta('is_hosting_plan') : 'off',
        ));


    woocommerce_wp_select(
        array(
            'id' => 'hosting_plan',
            'name' => 'hosting_plan',
            'label' => __('Hosting Plan', 'woocommerce'),
            'options' => get_hosting_plans(),
            'value' => $product->get_meta('hosting_plan') ? $product->get_meta('hosting_plan') : '',
        ));

    echo '</div>';
}

// Saving custom fields values from admin product settings
function save_admin_product_custom_fields_values($product)
{
    if (isset($_POST['hosting_plan'])) {
        $product->update_meta_data('hosting_plan', sanitize_text_field($_POST['hosting_plan']));
    }

    if (isset($_POST['is_hosting_plan'])) {
        $product->update_meta_data('is_hosting_plan', sanitize_text_field($_POST['is_hosting_plan']));
    }
}

