<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

function WPFossBilling_UserMeta()
{
    Container::make('user_meta', 'FOSSBILLING_User_Meta')
        ->add_fields(
            array(
                Field::make('text', 'fossbilling_client_id', 'FOSSBILLING Client ID'),
            )
        );
}