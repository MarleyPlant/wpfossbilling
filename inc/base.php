<?php
class FOSSBillingAPI_BASE {
    public $api_guest;
    public $api_client;
    public $api_admin;

    public function __construct($api_guest, $api_client, $api_admin) {
        $this->api_guest = $api_guest;
        $this->api_client = $api_client;
        $this->api_admin = $api_admin;
    }
}