<?php
class FOSSBillingAPI_Products extends FOSSBillingAPI_BASE {
    public function __construct($api_guest, $api_client, $api_admin) {
        parent::__construct($api_guest, $api_client, $api_admin);
    }

    public function getProducts() {
        return $this->api_guest->product_get_list()->list;
    }

    public function getProduct($id) {
        return $this->api_guest->product_get($id);
    }

    public function orderProduct($data) {
        return $this->api_client->product_order($data);
    }
}