<?php

class FOSSBillingAPI_Payments extends FOSSBillingAPI_BASE {
    public function __construct($api_guest, $api_client, $api_admin) {
        parent::__construct($api_guest, $api_client, $api_admin);
    }

    public function getPayments() {
        return $this->api_guest->payment_get_list()->list;
    }
}