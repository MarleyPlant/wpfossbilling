<?php
class FOSSBillingAPI_Clients extends FOSSBillingAPI_BASE {
    public function __construct($api_guest, $api_client, $api_admin) {
        parent::__construct($api_guest, $api_client, $api_admin);
    }

    public function getClients() {
        return $this->api_admin->client_get_list()->list;
    }

    public function getClient($id) {
        return $this->api_admin->client_get($id);
    }
    
    public function createClient($data) {
        return $this->api_admin->client_create($data);
    }

    public function updateClient($id, $data) {
        return $this->api_admin->client_update($id, $data);
    }

    public function deleteClient($id) {
        return $this->api_admin->client_delete($id);
    }
}

