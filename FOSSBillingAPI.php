<?php
include_once 'inc/base.php';
include_once 'inc/service.php';
include_once 'inc/controllers/clients.php';
include_once 'inc/controllers/products.php';


class FOSSBillingAPI
{
    private $config;
    private $api_guest;
    private $api_admin;

    private $api_client;

    public $clients;
    public $products;

    public $hosting;

    public $domains;

    public function __construct($config)
    {
        $this->config = $config;
        $this->api_guest = new Service_BoxBilling(['api_url' => $config['api_url'], 'api_role' => 'guest']);
        $this->api_admin = new Service_BoxBilling(['api_url' => $config['api_url'], 'api_token' => $config['api_token'], 'api_role' => 'admin']);
        $this->api_client = new Service_BoxBilling(['api_url' => $config['api_url'], 'api_token' => $config['api_token'], 'api_role' => 'client']);

        $this->clients = new FOSSBillingAPI_Clients($this->api_guest, $this->api_guest, $this->api_admin);
        $this->products = new FOSSBillingAPI_Products($this->api_guest, $this->api_guest, $this->api_admin);
    }
}
